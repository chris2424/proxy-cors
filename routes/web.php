<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Helpers\ProxyHelperFacade;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::middleware(['cors'])->group(function () {
//     Route::match(['get', 'post', 'head', 'patch', 'put', 'delete'] , 'proxy/{url}', function($url, Request $request){
//         return ProxyHelperFacade::CreateProxy($request)->toHost($url);
//     })->where('url', '(.*(?:%2F:)?.*)');
// });
