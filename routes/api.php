<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Helpers\ProxyHelperFacade;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::match(['get', 'post', 'head', 'patch', 'put', 'delete'] , 'proxy/{url}', function($url, Request $request){
    return ProxyHelperFacade::CreateProxy($request)->toHost($url);
})->where('url', '(.*(?:%2F:)?.*)');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
