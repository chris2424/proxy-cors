
<!DOCTYPE html>
<html lang="en">

<head>

  <title>CORS Proxy</title>

  <!--Head -->
  <!-- Begin Head -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Free Online Services. Github star history. Count Lines of Code. CORS proxy server. IP GeoLocation. Convert video to gif. HTTP Headers. Api weather temp. Alexa ranking." />

<!-- Favicon -->
<link rel="icon" type="image/x-icon" href="/_public/icons/ct/ct24r.png">
<!-- END-->

</head>

<body>

  <header class="header">
   
    <h1 class="big">
      <img class="verticalImage" alt="Logo" src="https://owldevs.id/assets/uploads/media-uploader/owldevs1616341017.png">
      <span>CORS Proxy Owldevs Team</span>
    </h1>
  </header>

  <!-- Version -->
  <!-- Version -->
<h2 class="centerText">
  API documents
  <small>
    <small>(version 0.1.0)</small>
  </small>
</h2>
<!-- End  -->


  <section class="description">
    <div class="centerText">
      <strong>Description</strong>
    </div>
    <div class="leftText">
      <span>
        - Free CORS proxy server to bypass same-origin policy related to performing standard AJAX requests to 3rd party
        services.<br>
        You can use to prevent mixed content of images and JSON data proxying the resources to serve them under https.
      </span>
      <br>
      <span>
        - API URL =>
        <code class="red">"https://proxy.owldevs.id/proxy/url_to_http_resource>"</code>
      </span>
      <br>
      <!-- <span>
        - Each request is limited to Access size download to avoid abuse.
      </span> -->
      <br>
      <span>
        - Only suppports GET request.<br>
      </span>
      <span>
     
        <!-- <span class="red">Limit</span> : 5 request per second. Once reached subsequent requests will result in error
        429 (too many requests)
        until your quota is cleared.
        <br> -->
      </span>
    </div>
  </section>

  <section class="endpoint old">
    <div class="centerText">
      <strong>Endpoint - Do cors proxy</strong>
    </div>
    <div class="leftText">
      <strong>http Request : </strong>
      <br>
      <code class="red">
    GET https://proxy.owldevs.id/proxy/url_to_http_resource> 
      </code>
      <br>
      <strong>Examples</strong>
      <br>Access resources from other website :
      <br>
      <span>
        <code>
  <a href="https://proxy.owldevs.id/api/proxy/https://api.kawalcorona.com/indonesia/"> https://proxy.owldevs.id/api/proxy/url_to_http_resource></a> 
        </code>
      </span>
      <br>Serve an http image as https preventing mixed content :
      <br>
      <span>
        <code>
<a href="https://proxy.owldevs.id/api/proxy/https://sa/_public/icons/jolav128.png"> https://proxy.owldevs.id/api/proxy/url_to_http_resource></a> 
        </code>
      </span>
    </div>
    </div>
  </section>

  <br>

  <!-- Footer -->
  <!-- Begin Footer-->
<style>
  .footer {
    font-family: "arial", sans-serif;
    text-align: center;
    font-size: 0.95em;
    line-height: 1.2em;
  }

  html {
    height: 100%;
    box-sizing: border-box;
  }

  *,
  *:before,
  *:after {
    box-sizing: inherit;
  }

  body {
    position: relative;
    margin: 0 auto;
    min-height: 98%;
    padding-bottom: 6rem;
  }

  .footer {
    position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    padding: 1rem;
    text-align: center;
  }
</style>

<footer class="footer">
  <hr>
  <a href="//owldevs.id">
    <img class="verticalImage" alt="goHome" src="https://owldevs.id/assets/uploads/media-uploader/favicon1616341154.png"> <strong>owldevs.id</strong>
  </a>
  <br>
  <span>You can submit your questions, feedbacks, and feature requests opening a issue on github or
    <a href="mailto:contact@owldevs.id">
      <strong>emailing us</strong>
    </a>
  </span>
  <br>
  <img class="logo" style="max-height:32px; vertical-align:middle" alt="" src="/_public/icons/ct/jolav32.png">
  Owldevs.id
  &copy; 2021 -
  <strong>
    <a href="https://gitlab.com/chris2424/proxy-cors">View on GITLab</a>
  </strong>
  <hr>
</footer>
<br>
<!-- End -->


  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="https://codetabs.com/apis.css">

  <!-- google-analytics -->
  <!-- Google Analytics -->


</body>

</html>
